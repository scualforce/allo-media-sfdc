/**
* @author Pascal GUILLÉN (scual)
* @date 01/12/2019
* @description {ApexClass} QueueableRequestCalls
* @testclass {ApexClass} QueueableRequestCalls_test
*/
@istest(SeeAllData=false)
public class QueueableRequestCalls_test {
    @testsetup
    static void setup(){
        TestHelper.CreateCall();
    }
    @istest
    static void testLastCallId(){
        string Callid = [select CallId__c FROM Call__c limit 1].CallId__c;
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestHelper.AlloMediaMockSuccess());
        System.enqueueJob(new QueueableRequestCalls(Callid));
        test.stopTest();
    }
    @istest
    static void testFullBase(){
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestHelper.AlloMediaMockSuccess());
        System.enqueueJob(new QueueableRequestCalls());
        test.stopTest();
    }
    @istest
    static void testReload(){
        test.startTest();
        Call__c[] Calls  = [select CallId__c FROM Call__c limit 1];
        Calls[0].id=null;
        Test.setMock(HttpCalloutMock.class, new TestHelper.AlloMediaMockSuccess());
        System.enqueueJob(new QueueableRequestCalls(0,null,JSON.serialize(Calls)));
        test.stopTest();
    }
}