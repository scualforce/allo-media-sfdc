/*
* @author Pascal GUILLÉN (scual)
* @date 01/12/2019
* @description {ApexClass} BatchLinkCall
* @testclass {ApexClass} BatchLinkCall_test
*/
@istest(SeeAllData=false)
public class TestHelper {
    public class AlloMediaMockFailure implements HttpCalloutMock{
        //Implement http mock callout failure here 
        public HTTPResponse respond(HTTPRequest request) {
            // Create a fake response
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('YOU FAIL');
            response.setStatusCode(500);
            response.setStatus('allo-media server error');
            return response; 
        }
    }
    public class AlloMediaMock404 implements HttpCalloutMock{
        //Implement http mock callout failure here 
        public HTTPResponse respond(HTTPRequest request) {
            // Create a fake response
            HttpResponse response = new HttpResponse();
            response.setStatusCode(404);
            return response; 
        }
    }
     public class AlloMediaMockUnauthorized implements HttpCalloutMock{
        //Implement http mock callout failure here 
        public HTTPResponse respond(HTTPRequest request) {
            // Create a fake response
            HttpResponse response = new HttpResponse();
            response.setStatusCode(401);
            response.setBody('Error: Unauthorized');
            return response; 
        }
    }  
    public class AlloMediaMockSuccess implements HttpCalloutMock{
        //Implement http mock callout failure here 
        public HTTPResponse respond(HTTPRequest request) {
            // Create a fake response
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(WS_RestApiCalls_test.mockcallout());
            response.setStatusCode(200);
            return response; 
        }
    }
    public static string CreateCall(){
        string callId = '1675471121.7704341';
        insert new call__c(
            Callid__c = callId,
            ServerId__c = '505',
            DateCall__c = Datetime.now(), 
            Phone__c = '066060606', 
            Duration__c= '1m 23s'
        );
        return callId;
    }
    public static void CreateAccount(){
        List<Account> accs = new List<Account>();
        accs.add(new Account(name='FR Internation',phone='+33606060601'));
        accs.add(new Account(name='FR Local',phone='0606060603'));
        accs.add(new Account(name='USA Internation',phone='+15606060602'));
        accs.add(new Account(name='USA Local',phone='5506060604'));
        accs.add(new Account(name='FR Internation',phone='+33606060601'));
        insert accs;
        
        List<Contact> ctts = new List<Contact>();
        ctts.add(new Contact(LastName='FR Internation',phone='+33606060601'));
        ctts.add(new Contact(LastName='FR Local',phone='33606060603'));
        ctts.add(new Contact(LastName='USA Internation',phone='+5506060604'));
        ctts.add(new Contact(LastName='USA Local',phone='+33606060604'));
        insert ctts;
    }

}