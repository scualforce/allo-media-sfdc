/*
* @author Pascal GUILLÉN (scual)
* @date 10/01/2020
* @description {ApexClass} DataRecovery_test
*/
@istest(SeeAllData=false)
public with sharing class DataRecovery_test {
    @testsetup
    static void setup(){
        TestHelper.CreateCall();
    }
    @istest
    public static void test_Partial_OK() {
        delete [select id from Call__c];
        DataRecovery.partial('550-1575471121.7704341');
    }
    @istest
    public static void test_Partial_KO() {
        try{
            DataRecovery.partial('550-1575471121.7704341');
        }catch(Exception e){
            System.debug('Exception trig is ok');
        }        
    }
    @istest
    public static void test_classic() {
        DataRecovery.classic();
    }
    @istest
    public static void test_batch() {
        DataRecovery.batch();
    }
    @istest
    public static void test_Full_OK() {
        delete [select id from Call__c];
        DataRecovery.Full();
    }
    @istest
    public static void test_Full_KO() {
        try{
            DataRecovery.Full();
        }catch(Exception e){
            System.debug('Exception trig is ok');
        }        
    }
}
