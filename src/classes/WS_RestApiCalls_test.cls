/*
* @author Pascal GUILLÉN (scual)
* @date 01/12/2019
* @description {ApexClass} BatchLinkCall
* @testclass {ApexClass} BatchLinkCall_test
*/
@istest
public class WS_RestApiCalls_test {
    
    @istest(SeeAllData=false)
    static void testOk(){
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestHelper.AlloMediaMockSuccess());
        WS_RestApiCalls ws = new WS_RestApiCalls(Constants.MaximalCallsByRequest);
        integer offset = 0;
        while(!ws.IsFinish){
            ws.getResults(offset);
            offset+=Constants.MaximalCallsByRequest;
        }
        test.stopTest();
    }
    @istest
    static void testFail(){
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestHelper.AlloMediaMockFailure());
        WS_RestApiCalls ws = new WS_RestApiCalls(Constants.MaximalCallsByRequest);
        try{
            ws.getResults(0);
        } catch(Exception ex){}
        test.stopTest();
    }
    @istest
    static void test404(){
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestHelper.AlloMediaMock404());
        WS_RestApiCalls ws = new WS_RestApiCalls(Constants.MaximalCallsByRequest);
        try{
            ws.getResults(0);
        } catch(Exception ex){}        
        test.stopTest();
    }
    @istest
    static void AlloMediaMockUnauthorized(){
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new TestHelper.AlloMediaMockUnauthorized());
        WS_RestApiCalls ws = new WS_RestApiCalls(Constants.MaximalCallsByRequest);
        try{
            ws.getResults(0);
        } catch(Exception ex){}        
        test.stopTest();
    }
    public static string mockcallout(){
        return [select body from staticResource where name='testdata'].body.toString();
    }
}