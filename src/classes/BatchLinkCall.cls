/*
* @author Pascal GUILLÉN (scual)
* @date 01/12/2019
* @description {ApexClass} BatchLinkCall
* this batch used to link all Call with Acount,contact or all custom SObject you choose.
*
* @testclass {ApexClass} BatchLinkCall_test
*/
global without sharing class  BatchLinkCall implements Database.Batchable<sObject>{

    private Map<id,call__c> CallsToUpMap = new Map<id,call__c>();
    global Database.QueryLocator start(Database.BatchableContext bc) {
        // can't Group by on mdt :L'opération demandée n'est pas encore prise en charge par ce type de stockage SObject. Pour plus d'informations, contactez le support salesforce.com
        set<string> fields = new set<string>();
        for(ConfigMatchingPhone__mdt config:[SELECT CallLookupField__r.QualifiedAPIName  FROM ConfigMatchingPhone__mdt WHERE IsActive__c = true]){
            fields.add(config.CallLookupField__r.QualifiedAPIName);
        }
        string querySelect ='select id,Phone__c';
        // request: loop on all Dynamic LookUp field
        for(string field:fields){
            querySelect += ','+field;
        }
        // Criteria: loop on all Dynamic LookUp field
        string queryWhere = '';
        for(string field:fields){
            queryWhere += ' OR '+field+' = null';
        }
        string query = querySelect + ' FROM Call__c WHERE' + queryWhere.substring(3);
        System.debug('query=' + query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Call__c> Calls){
        // list number phone to use for criteria in all Sobjects used
        set<string> numPhones = new set<string>();
        // Map of list Call__c Sobject by internation number phone
        Map<string,List<Call__c>> callsMap = new Map<string,List<Call__c>>();
        for(Call__c call:Calls) {
            if(call.Phone__c != null){
                // add on list international number phone
                numPhones.add(call.Phone__c);
                // add on list international number phone by State (FR,USA)                
                if(call.Phone__c.startswith('+33')){
                    // manager FR phone number
                    numPhones.add(call.Phone__c.replace('+33','0'));
                } else if(call.Phone__c.startswith('+1')){
                    // manager USA phone number
                    numPhones.add(call.Phone__c.replace('+1',''));
                } 
                // put list call by number phone
                List<Call__c> callsSameNum = CallsMap.get(call.Phone__c);
                if(callsSameNum == null){
                    callsSameNum = new List<Call__c>();
                    callsMap.put(call.Phone__c,callsSameNum);
                }
                callsSameNum.add(call);
            }
        }
        if(numPhones.size()>0){
            //request configs fields phone By SObject
            Map<string,List<ConfigMatchingPhone__mdt>> targetPhoneFldByObj = getTargetPhoneByObject();
            for(string objetName:targetPhoneFldByObj.keySet()){
                List<ConfigMatchingPhone__mdt> fieldMapping = targetPhoneFldByObj.get(ObjetName);
                setlinkByObject(objetName,fieldMapping,numPhones,callsMap);
            }
            update this.CallsToUpMap.values();
        }        
    }    
    global void finish(Database.BatchableContext BC) {

    }
    /*
    * @author Pascal GUILLÉN (scual)
    * @date 01/12/2019
    * @description method used for request configs fields phone By SObject
    * @return {Map<string,List<ConfigMatchingPhone__mdt>>} results Map of list of fields phone By SObject
    */
    private Map<string,List<ConfigMatchingPhone__mdt>> getTargetPhoneByObject(){
        //group Dynamic field in a map by the same Dynamic SObject type
        Map<string,List<ConfigMatchingPhone__mdt>> targetPhoneFldByObj = new Map<string,List<ConfigMatchingPhone__mdt>>();
        for(ConfigMatchingPhone__mdt Config : [SELECT FieldPhone__r.QualifiedAPIName, ObjectType__r.QualifiedAPIName,CallLookupField__r.QualifiedAPIName FROM ConfigMatchingPhone__mdt where IsActive__c=true]){
            List<ConfigMatchingPhone__mdt> Fields = targetPhoneFldByObj.get(Config.ObjectType__r.QualifiedAPIName);
            if(Fields == null){
                Fields = new List<ConfigMatchingPhone__mdt>();
                targetPhoneFldByObj.put(Config.ObjectType__r.QualifiedAPIName,Fields);
            }
            Fields.add(config);
        }
        return targetPhoneFldByObj;
    }
    /*
    * @author Pascal GUILLÉN (scual)
    * @date 01/12/2019
    * @description method used for set the lookUp parent (Account,Contact or Custom object)
    * @param {String} ObjName, SObject Name
    * @param {ist<ConfigMatchingPhone__mdt>} fields, list of phone number from the SObject
    * @throws {DmlException}
    * @return {Boolean} true is successful
    */
    private void setlinkByObject(String ObjName,List<ConfigMatchingPhone__mdt> Fields,Set<String> PhonesNum,Map<string,List<Call__c>> CallsMap){
        //create query with SObject et dynamic field and Phones Number Criteria field
        String querySelect = 'SELECT Id';
        //request: loop on all Dynamic phone field
        for(ConfigMatchingPhone__mdt field:Fields){
            querySelect += ',' + field.FieldPhone__r.QualifiedAPIName;
        }
        // Criteria loop on all Dynamic phone field
        String CritereWhere = '';
        for(ConfigMatchingPhone__mdt field:Fields){
            CritereWhere += 'OR ' + field.FieldPhone__r.QualifiedAPIName + ' in :PhonesNum ';
        }
        //remove first OR
        string query = querySelect + ' FROM ' + ObjName + ' WHERE '+ CritereWhere.substring(3);

        System.debug(query);
        for(SObject obj : (List<SObject>)Database.query(query)){
            for(ConfigMatchingPhone__mdt FieldPhone : Fields){
                String phone = (String)obj.get(FieldPhone.FieldPhone__r.QualifiedAPIName);
                if(phone != null) {
                    //phone Local => phone Internation
                    if(!phone.startswith('+')){
                        if(phone.startswith('0')) {
                            // manager FR phone number
                            phone='+33'+phone.substring(1);
                        } else {
                            // manager USA phone number
                            phone='+1'+phone;
                        }
                    }
                    // Check if this phone contains in the map of call
                    List<Call__c>  calls = CallsMap.get(phone);
                    if(calls != null){
                        //loop call have the same phone number
                        for (Call__c call : calls) {
                            String lookup = (String) call.get(FieldPhone.CallLookupField__r.QualifiedAPIName);
                            if(lookup == null){
                                //if the lookUp is not already populate then set it 
                                call.put(FieldPhone.CallLookupField__r.QualifiedAPIName,obj.Id);
                            }
                            this.CallsToUpMap.put(call.Id,call);
                        }
                    }
                }                
            }            
        }
    }    
}