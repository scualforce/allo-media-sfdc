/*
* @author Pascal GUILLÉN (scual)
* @date 01/12/2019
* @description {ApexClass} Constants
* @testclass {ApexClass} Constants_test
*/
public without sharing class Constants {
    
    // enum type organization
    public enum OrgType {
        Sandbox,
        DevEdition,
        Production        
    }
    // max items return by request from ws allo-media
    public static final Integer MaximalCallsByRequest = 100;
    /*
    * @author Pascal GUILLÉN (scual)
    * @date 01/12/2019
    * @return {OrgType} the type of the organization
    */
    public static final OrgType MyOrgType = GetOrgType();
    public static OrgType GetOrgType() {
        OrgType MyOrgType = OrgType.Production;
        Organization org = [SELECT Id, IsSandbox, OrganizationType FROM Organization limit 1];
        if(org.IsSandbox){
            MyOrgType =  OrgType.Sandbox;
        } else if(org.OrganizationType == 'Developer Edition'){
            MyOrgType =   OrgType.DevEdition;
        }
        system.debug('OrganizationType :'+org.OrganizationType);
        return MyOrgType;
    }
    /*
    * @author Pascal GUILLÉN (scual)
    * @date 01/12/2019
    * @return {OrgType} the limit by the organization, and limit specific for DevEdition
    */
    public static Integer getLimitQueueableJobs(){
        Integer LimitQueueableJobs = limits.getLimitQueueableJobs();
        if(MyOrgType== OrgType.DevEdition){
            LimitQueueableJobs = 5;
        }
        return LimitQueueableJobs;
    }
    // limit Callouts specific for Running Test
    public static Integer LimitCallouts { 
        get{
            if(Test.isRunningTest()){
                return 3;
            } else {
                return limits.getLimitCallouts();
            }
        }
    } 
    // exclure field in the result from ws allo-media
    public static final string URL_PARAM_EXCLUDE ='transcript_json,qualification,out,visit,tracking_info,status';

    public static final string ERROR_SERVER = 'SERVER ALLO-MEDIA [ERROR SERVER]';
    public static final string ERROR_NOT_FOUND = 'SERVER ALLO-MEDIA [SERVER NOT FOUND]';
    public static final string ERROR_UNAUTHORIZED = 'SERVER ALLO-MEDIA [UNAUTHORIZED]';

    public static final Integer MAXSIZE_ARRAY_SOBJECT = 10000;
    // Cputime save to secure end process (millisecond=
    public static final Integer CPUTIME_ENDPROCESS = 15000;
    // HeapSize save to secure end process (bit)
    public static final Integer HEAPSIZE_ENDPROCESS = 1000000;
    
}