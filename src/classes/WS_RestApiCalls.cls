/*
* @author Pascal GUILLÉN (scual)
* @date 01/12/2019
* @description {ApexClass} WS_RestApiCalls
* @testclass {ApexClass} WS_RestApiCalls_test
*/
public without sharing class WS_RestApiCalls {
    public class RestApiException extends Exception{}
    public Class call {
        public string uniqueId;
        public string serverId;
        public string callId;
        public Datetime dtCall;
        public string duration;
        public string campagne;
        public string Phone;
    }
    //max call Object bt request
	private integer Maximum;
    //total number call Object on the serveur
    private integer Count;
    public boolean IsFinish {get;private set;}
    //excluding return fields set on the parameter GET request
    public final string URL_PARAM_EXCLUDE = Constants.URL_PARAM_EXCLUDE;
    /*
    * @author Pascal GUILLÉN (scual)
    * @date 01/12/2019
    * @param {String} str, brieve description 
    */
    public WS_RestApiCalls(integer Maximum){
        this.Maximum = Maximum;
        IsFinish = false;
    }
    /*
    * @author Pascal GUILLÉN (scual)
    * @date 01/12/2019
    * @description method used for requested by GET REST the backend allo-media
    * @param {integer} offset, position of the cursor
    * @throws {RestApiException} Throws if StatusCode not begin 2 ex: 200, 201, 203
    * @return {String} JsonString requested Call object
    */
    private string callout(integer offset){
        //system.debug(logginglevel.FINEST,'WS_RestApiCalls.callout[Maximum='+Maximum+']|offset='+offset+']');
        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:allomedia__wsAlloMedia/calls?limit='+Maximum+'&offset='+offset+'&exclude='+URL_PARAM_EXCLUDE);
        req.setHeader('accept','application/json');
        req.setHeader('Authorization','Token {!$Credential.Password}');
        req.setMethod('GET');
        req.setTimeout(60000);
        Http http = new Http();
        HTTPResponse res = http.send(req);
        //Throws if StatusCode not begin 2 ex: 200, 201, 203
        system.debug('getEndpoint:'+req.getEndpoint());
        string code = (''+res.getStatusCode()).substring(0,1);
        if(code != '2'){
            string messageCode = '';
            if(code == '5'){
                messageCode = Constants.ERROR_SERVER; 
            } else if(code == '4'){
                if(res.getStatusCode()==401){
                    messageCode = Constants.ERROR_UNAUTHORIZED;
                } else if(res.getStatusCode()==404){
                    messageCode = Constants.ERROR_NOT_FOUND;
                }                 
            }
            messageCode += ' Status => Code='+res.getStatusCode()+', Message='+res.getStatus();
            system.debug(logginglevel.FINEST,messageCode);
            throw new RestApiException(messageCode);
        }
        System.debug(res.getBody());
        return res.getBody();
    }
    /*
    * @author Pascal GUILLÉN (scual)
    * @date 01/12/2019
    * @description method used for deserialize the request result
    * ex:{
            "count":1,
            "data":[{
                "unique_id":"550-1575471121.7704341"
                "in":{
                    "type":"PHONE",
                    "data":"+33606060606"
                },
                "timestamp"="2019-12-04T15:00:00Z",
                "campagne"="importer un lot",
                "duration"=68,
            }]
        }
    * @param {String} jsonInput, position of the cursor
    * @return {List<call>} results array of Call object requested
    */
    private List<call> deserialize(String jsonInput){
        Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(jsonInput);
        Count = (integer)m.get('count');
        List<call> Calls = new List<call>();
        for(Object item:(List<Object>)m.get('data')){
            Map<String, Object> callOjb = (Map<String, Object>)item;
            Map<String, Object> inty = (Map<String, Object>)callOjb.get('in');
            // search Phone number
            string Phone = (string)inty.get('data');
            // type must be PHONE
            if((string)inty.get('type')=='PHONE' && !string.isEmpty(Phone)){
                call myCall = new call();
                myCall.uniqueId = (string)callOjb.get('unique_id');
                List<string> sections = myCall.uniqueId.split('-',-1);                
                myCall.serverId = sections[0];
                myCall.CallId = sections[1];
                string timestamp = '"'+(string)callOjb.get('timestamp')+'"';
                myCall.dtCall = (Datetime)JSON.deserialize(timestamp, Datetime.class);
                myCall.phone = Phone;
                // get Duration
                myCall.campagne = (string)callOjb.get('campagne');
                // get Duration
                integer duration = Integer.valueOf(callOjb.get('duration'));
                if(duration>0){
                    integer hour = (integer)duration/3600;
                    integer tmpsMinute = math.mod(duration, 3600);
                    integer Minute = (integer)tmpsMinute/60;
                    Integer second = math.mod(tmpsMinute, 60);
                    myCall.duration = ((hour>0)?(hour+'h '):'')+((Minute>0)?(Minute+'m '):'')+second+'s';
                }
                Calls.add(myCall);
            }
        }
        return calls;
    }
    /*
    * @author Pascal GUILLÉN (scual)
    * @date 01/12/2019
    * @description method used for enclose  callout, deserialize and check end request.
    * @param {integer} offset, position of the cursor
    * @param {String} jsonInput, position of the cursor
    * @return {List<call>} results array of Call object requested
    */
    public List<call> getResults(integer offset){
        system.debug(logginglevel.FINEST,'WS_RestApiCalls.getResults Begin');
        List<call> Calls = new List<call>();
        string jsonResults = this.callout(offset);
        Calls = this.deserialize(jsonResults);
        system.debug(logginglevel.DEBUG,'Count='+Count+' offset='+offset+' Maximum='+Maximum);
        if(Count<offset+Maximum){
            IsFinish = true;
        }
        system.debug(logginglevel.FINEST,'WS_RestApiCalls.getResults End');
        return Calls;
    }
    /*
    * @author Pascal GUILLÉN (scual)
    * @date 01/12/2019
    * @description method used for enclose  callout, deserialize and check end request.
    */
    public static void check(){
        WS_RestApiCalls ws = new WS_RestApiCalls(50);
        try{
            list<WS_RestApiCalls.call> callResults = ws.getResults(0);
            System.debug('Token is OK');
            System.debug(Json.serializePretty(callResults[0]));
        } catch(RestApiException ex){
            System.debug(ex.getMessage());
        } catch(Exception ex){
            System.debug(ex.getMessage());
        }
    }
}