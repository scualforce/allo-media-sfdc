/*
* @author Pascal GUILLÉN (scual)
* @date 01/12/2019
* @description {ApexClass} BatchLinkCall
* @testclass {ApexClass} BatchLinkCall_test
*/
@istest(SeeAllData=false)
public class BatchLinkCall_test {
    static string Callid;
    @testsetup
    static void setup(){
        Callid = TestHelper.CreateCall();
    }
    @istest
    static void test(){
        test.startTest();
        BatchLinkCall c = new BatchLinkCall();
        Database.executeBatch(c);
        test.stopTest();
    }
}