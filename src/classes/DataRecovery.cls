global with sharing class DataRecovery {
     global static string Partial(string unique_id){
        integer count = Database.countQuery('SELECT count() FROM call__c');
        if(count>0){
            throw new QueueableRequestCalls.AppException('Vous ne pouvez pas utiliser cette methode, car il existe déja des données dans la table Call__c.');
        } else{
            string LastUniqueId = unique_id.split('-', 2)[1];
            return System.enqueueJob(new QueueableRequestCalls(0,LastUniqueId,null));
        }
    }
    global static string Full(){
        integer count = Database.countQuery('SELECT count() FROM call__c');
        if(count>0){
            throw new QueueableRequestCalls.AppException('Vous ne pouvez pas utiliser cette methode, car il existe déja des données dans la table Call__c.');
        } else{
            return System.enqueueJob(new QueueableRequestCalls(0,null,null));
        }
    }
    global static string classic(){
        string LastUniqueId = null;
        AggregateResult[] groupedResults = [SELECT Max(callId__c)lastId FROM call__c ];
        if(groupedResults.size()>0){
            LastUniqueId = (string)groupedResults[0].get('lastId');
        }
        return System.enqueueJob(new QueueableRequestCalls(0,LastUniqueId,null));
    }

    global static string Batch(){
        return Database.executeBatch(new BatchLinkCall());
    }       
}
