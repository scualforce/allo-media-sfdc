/*
* @author Pascal GUILLÉN (scual)
* @date 01/12/2019
* @description {ApexClass} ScheduledRequestCall
* @testclass {ApexClass} ScheduledRequestCall_test
*/
global class ScheduledRequestCall implements Schedulable{
    global void execute(SchedulableContext SC) {
        string JobId = DataRecovery.classic();
        System.debug('JobId='+JobId);
    }
}