/*
* @author Pascal GUILLÉN (scual)
* @date 01/12/2019
* @description {ApexClass} ScheduledRequestCall
* @testclass {ApexClass} ScheduledRequestCall_test
*/
@istest(SeeAllData=false)
public class ScheduledRequestCall_test {
    @istest
    static void test() {
        test.startTest();
        String sch = '0 0 23 * * ?'; 
        Test.setMock(HttpCalloutMock.class, new TestHelper.AlloMediaMockSuccess());
        ScheduledRequestCall sh1 = new ScheduledRequestCall();
        system.schedule('Allo-Media Scheduled Request Calls', sch, sh1);
        test.stopTest();
    }
}
