/**
* @author Pascal GUILLÉN (scual)
* @date 01/12/2019
* @description {ApexClass} QueueableRequestCalls
* @testclass {ApexClass} QueueableRequestCalls_test
*/
global without sharing class QueueableRequestCalls implements Queueable, Database.AllowsCallouts {
    public class AppException extends Exception{}
    private integer Offset;
    private string LastUniqueId;
    // list Call sobject to insert by instance
    private List<call__c> Calls = new List<call__c>();
    // LimitQueueableJobs custom
    private final integer LimitQueueableJobs = constants.getLimitQueueableJobs();
    // max items return by request from ws allo-media
    private final integer MaximalCallsByRequest = Constants.MaximalCallsByRequest;
    // custom LimitCallouts custom
    private final integer LimitCallouts = Constants.LimitCallouts;
    public QueueableRequestCalls() {
        this(0,null,null);
    }
    public QueueableRequestCalls(string LastUniqueId) {
        this(0,LastUniqueId,null);
    }
    public QueueableRequestCalls(integer offset,string LastUniqueId, string strJsonCalls){
        system.debug(logginglevel.FINEST,'QueueableRequestCalls Begin');
        this.offset = offset;
        this.LastUniqueId = LastUniqueId;
        if(strJsonCalls!=null){
            system.debug(logginglevel.FINEST,'strJsonCalls='+strJsonCalls);
            this.Calls = (List<call__c>)JSON.deserialize(strJsonCalls,List<call__c>.class);
        }
        system.debug(logginglevel.FINEST,'LastUniqueId='+this.LastUniqueId);
        system.debug(logginglevel.FINEST,'QueueableRequestCalls End');
    }
    global void execute(QueueableContext context) {
        system.debug(logginglevel.FINEST,'QueueableRequestCalls execute Begin');
        system.debug(logginglevel.FINEST,'LastUniqueId='+this.LastUniqueId);
        boolean isLastUniqueId = false;
        boolean isSizeMaxArrayCalls = false;
        boolean isLimitCallouts = false;
        boolean isLimitCpuTime = false;
        boolean isLimitHeapSize = false;
        try {
            WS_RestApiCalls ws = new WS_RestApiCalls(MaximalCallsByRequest);
            // Sortir de la boucle for Cycle si :
            //- Tous les appelles sont gérer
            //- OU on n'as pas atteint le dernier call déjà traitement ultérieurement.
            //- on as
            while (!ws.isFinish && !isLastUniqueId && !isLimitCallouts && !isSizeMaxArrayCalls && !isLimitCpuTime && !isLimitHeapSize) {
                list<WS_RestApiCalls.call> callResults = ws.getResults(Offset);
                for(WS_RestApiCalls.call myCall:callResults){
                    //system.debug(logginglevel.FINEST,'LastUniqueId='+this.LastUniqueId+' myCall.callId='+myCall.callId);
                    if(LastUniqueId == null || LastUniqueId != myCall.callId){
                        //ajouter les calls tan
                        Calls.add(
                            new call__c(
                                campagn__c = myCall.campagne,
                                Callid__c= myCall.callId,
                                ServerId__c = myCall.serverId,
                                DateCall__c = myCall.dtCall, 
                                Phone__c = myCall.Phone, 
                                Duration__c= myCall.Duration
                            )
                        );
                    } else {
                        // sortir de la boucle for Cycle si on a rencontrer le dernier CallId à été trouver
                        isLastUniqueId = true;
                        break;
                    } 
                }
                system.debug(logginglevel.FINEST,'Calls.size()='+Calls.size()+' MaximalCallsByRequest='+Calls.size()+MaximalCallsByRequest);
                isSizeMaxArrayCalls = Calls.size()+MaximalCallsByRequest > Constants.MAXSIZE_ARRAY_SOBJECT;
                isLimitCallouts = limits.getCallouts()>=LimitCallouts;
                isLimitCpuTime = limits.getCpuTime()+Constants.CPUTIME_ENDPROCESS>=limits.getlimitCpuTime();
                isLimitHeapSize = limits.getHeapSize()+Constants.HEAPSIZE_ENDPROCESS>=limits.getlimitHeapSize();
                Offset += MaximalCallsByRequest;
                system.debug(logginglevel.FINE,'HeapSize='+limits.getHeapSize()+'/'+limits.getlimitHeapSize()+'=> isLimitHeapSize='+isLimitHeapSize);
                system.debug(logginglevel.FINE,'CpuTime='+limits.getCpuTime()+'/'+limits.getlimitCpuTime()+'=> isLimitCpuTime='+isLimitCpuTime);
                system.debug(logginglevel.FINE,'Callouts='+limits.getCallouts()+'/'+limits.getLimitCallouts()+'=> isLimitCallouts='+isLimitCallouts);
                
            }
            
            integer QueueableJobs = limits.getQueueableJobs();
            system.debug(logginglevel.FINE,'QueueableJobs='+limits.getQueueableJobs()+'/'+limits.getLimitQueueableJobs());

            // Insertion de l'array de nouveau call
            // SI l'array est à la limit du max soit 10000
            // OU on a atteint le dernier call déjà traitement ultérieurement.
            // OU on n'est pas arrive à la fin de calls stock dans le serveur allo média
            system.debug(logginglevel.FINEST,'isSizeMaxArrayCalls='+isSizeMaxArrayCalls+' isFinish='+ws.isFinish+' isLimitCallouts='+isLimitCallouts+' isLastUniqueId='+isLastUniqueId);
            if(isSizeMaxArrayCalls || isLastUniqueId || ws.isFinish || isLimitCallouts){
                insert this.Calls;
            }

            // Relancement de d'un cycle de traitement
            // SI on n'est pas arrive à la fin de calls stock dans le serveur allo média
            // ET on n'as pas atteint le dernier call déjà traitement ultérieurement.
            // ET que la limits de queue n'est pas atteints
            system.debug(logginglevel.FINEST,'isLastUniqueId='+isLastUniqueId+' isFinish='+ws.isFinish);
            if(!ws.isFinish && QueueableJobs <= LimitQueueableJobs && !isLastUniqueId){
                //Hack best practice for test , see the documentation
                if(!Test.isRunningTest()) System.enqueueJob(new QueueableRequestCalls(offset,LastUniqueId,(!isSizeMaxArrayCalls)?Json.serialize(this.Calls):null));
            } else {
                // Traitement des requete des Call Terminer
                // lancement du batch
                BatchLinkCall c = new BatchLinkCall();
                Database.executeBatch(c);
            }
        } catch(WS_RestApiCalls.RestApiException e) {
            string message = 'SERVER ALLO-MEDIA '+ e.getmessage();
            throw new AppException(+message );
        }  catch(Exception e) {
            string message = 'SERVER ALLO-MEDIA ' + 'FALAL ERROR :'+e.getMessage();
            throw new AppException(message);
        }
        system.debug(logginglevel.FINEST,'QueueableRequestCalls execute End');
    }
}